class AverageRatingCreator
  def initialize(params)
    @post_id = params[:post_id]
    @number = params[:number]
  end

  def call
    ActiveRecord::Base.transaction do
      find_post
      create_rating
      fetch_average_rating
      update_post_average_rating
      @average_rating
    end
  rescue ActiveRecord::RecordNotFound, ActiveRecord::StatementInvalid => exception
    exception.message
  end

  private

  def find_post
    @post = Post.find(@post_id)
  end

  def create_rating
    Rating.create!(post: @post, number: @number)
  end

  def fetch_average_rating
    @average_rating = Rating.where(post: @post).average(:number).to_f
  end

  def update_post_average_rating
    @post.update!(average_rating: @average_rating)
  end
end
