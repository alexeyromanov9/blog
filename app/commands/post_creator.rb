class PostCreator
  def initialize(params)
    @title = params[:title]
    @body = params[:body]
    @login = params[:login]
    @ip = params[:ip]
  end

  def call
    ActiveRecord::Base.transaction do
      find_or_create_user
      find_or_create_ip_address
      create_post
    end
  rescue ActiveRecord::RecordNotFound, ActiveRecord::StatementInvalid => exception
    exception.message
  end

  private

  def find_or_create_user
    @user = User.find_or_create_by!(login: @login)
  end

  def find_or_create_ip_address
    IpAddress.find_or_create_by!(ip: @ip, user: @user)
  end

  def create_post
    Post.create!(title: @title, body: @body, ip: @ip, user: @user)
  end
end
