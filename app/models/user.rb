class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  has_many :ip_addresses, dependent: :destroy
end
