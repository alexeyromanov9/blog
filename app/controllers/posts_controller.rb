class PostsController < ApplicationController
  def create
    post_form = PostForm.new(permitted_params)
    if post_form.valid?
      post = PostCreator.new(post_form.attributes).call
      render json: post
    else
      render json: post_form.errors.full_messages, status: :unprocessable_entity
    end
  end

  def top
    top_form = TopRecordsForm.new(post_query_params)
    if top_form.valid?
      top_records = PostQuery.new(top_form.attributes).top_records_by_rating
      render json: top_records
    else
      render json: top_form.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def permitted_params
    params.permit(:title, :body, :login, :ip)
  end

  def post_query_params
    params.permit(:records_count)
  end
end
