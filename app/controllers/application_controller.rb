class ApplicationController < ActionController::Base
  rescue_from ArgumentError, with: :show_errors

  private

  def show_errors(exception)
    render json: exception.message, status: :unprocessable_entity
  end
end
