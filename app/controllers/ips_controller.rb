class IpsController < ApplicationController
  def multiple_users
    multiple_users_ips = IpQuery.new.fetch_multiple_user_ips
    ips_with_user = IpsPresenter.new(multiple_users_ips).ips_with_user_array
    render json: ips_with_user
  end
end
