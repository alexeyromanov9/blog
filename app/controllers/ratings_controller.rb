class RatingsController < ApplicationController
  def create
    rating_form = RatingForm.new(permitted_params)
    if rating_form.valid?
      average_rating = AverageRatingCreator.new(rating_form.attributes).call
      render json: average_rating
    else
      render json: rating_form.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def permitted_params
    params.permit(:post_id, :number)
  end
end
