class PostQuery
  def initialize(params)
    @records_count = params.fetch(:records_count)
  end

  def top_records_by_rating
    Post
      .select(:id, :title, :body, :average_rating)
      .order('average_rating DESC NULLS LAST')
      .limit(@records_count)
  end
end
