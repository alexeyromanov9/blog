class IpQuery
  def initialize(relation = IpAddress)
    @relation = relation
  end

  def fetch_multiple_user_ips
    @relation
      .joins(:user)
      .select('ip_addresses.ip', 'array_agg(users.login) as user_logins')
      .group('ip_addresses.ip')
      .having('count(users.login) > ?', 1)
  end
end
