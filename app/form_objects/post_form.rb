require 'resolv'

class PostForm < BaseForm
  attribute :title, String
  attribute :body, String
  attribute :login, String
  attribute :ip, String

  validates :title, :body, :login, :ip, presence: true
  validates :ip, format: Resolv::AddressRegex, allow_blank: true

  validate :login_uniqueness

  def login_uniqueness
    if User.exists?(login: login)
      errors.add(:login, 'is taken already!')
    end
  end
end
