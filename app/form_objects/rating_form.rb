class RatingForm < BaseForm
  attribute :number, Integer
  attribute :post_id, Integer

  validates :number, inclusion: { in: 1..5 }, numericality: { only_integer: true }
  validates :post_id, presence: true

  validate :post_present

  def post_present
    unless Post.exists?(id: post_id)
      errors.add(:post_id, 'is not present!')
    end
  end
end
