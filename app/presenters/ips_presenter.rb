class IpsPresenter
  def initialize(ip_addresses)
    @ip_addresses = ip_addresses
  end

  def ips_with_user_array
    @ip_addresses.inject([]) { |array, ip_address| array << [ip_address.ip.to_s, ip_address.user_logins] }
  end
end
