# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do
  User.create(login: Faker::Internet.email)
end

50.times do
  IpAddress.create(ip: Faker::Internet.ip_v4_address, user: User.first(50).sample)
end

200_000.times do
  Post.create(
    title: Faker::Lorem.word,
    body: Faker::Lorem.sentence,
    user: User.all.sample,
    ip: IpAddress.all.sample.ip,
    average_rating: Faker::Number.between(1.0, 5.0)
  )
end
