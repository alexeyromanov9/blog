class AddIpToPost < ActiveRecord::Migration[5.0]
  def up
    add_column :posts, :ip, :inet, null: false
  end

  def down
    remove_column :posts, :ip, :inet, null: false
  end
end
