class AddAverageRatingToPost < ActiveRecord::Migration[5.0]
  def up
    add_column :posts, :average_rating, :float
    add_index :posts, :average_rating, order: { average_rating: "DESC NULLS LAST" }
  end

  def down
    remove_column :posts, :average_rating, :float
  end
end
