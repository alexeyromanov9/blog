Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resource :posts, only: :create do
    get :top, on: :collection
  end
  resource :rating, only: :create
  get 'ips/multiple_users', to: 'ips#multiple_users'
end
