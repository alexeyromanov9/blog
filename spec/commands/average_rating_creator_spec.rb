require 'rails_helper'

describe AverageRatingCreator do
  let(:user) { User.create(login: 'login') }
  let(:post) { Post.create(user: user, title: 'title', body: 'body', ip: '194.158.213.76') }

  let(:valid_params) {{ post_id: post.id, number: 3 }}
  let(:invalid_param_post_id) { { post_id: 'ddd', number: 3 } }
  let(:invalid_param_number) { { post_id: post.id, number: 'nnn' } }
  let(:missed_param_post_id) { { number: 3 } }
  let(:missed_param_number) { { post_id: post.id } }

  let(:expected_average_rating) { 3.0 }
  let(:expected_averate_rating_with_invalid_number) { 0 }
  let(:expected_error_message) { "Couldn't find Post with 'id'=ddd" }
  let(:expected_error_message_with_missed_post_id) { "Couldn't find Post with 'id'=" }

  describe '#call' do
    context 'with valid_params' do
      subject { described_class.new(valid_params).call }
      it { is_expected.to eq expected_average_rating }
    end

    context 'with invalid_param_post_id' do
      subject { described_class.new(invalid_param_post_id).call }
      it { is_expected.to eq expected_error_message }
    end

    context 'with invalid_param_number' do
      subject { described_class.new(invalid_param_number).call }
      it { is_expected.to eq expected_averate_rating_with_invalid_number }
    end

    context 'with missed_param_post_id' do
      subject { described_class.new(missed_param_post_id).call }
      it { is_expected.to eq expected_error_message_with_missed_post_id }
    end

    context 'with missed_param_number' do
      subject { described_class.new(missed_param_number).call }
      it { is_expected.to eq expected_averate_rating_with_invalid_number }
    end
  end
end
