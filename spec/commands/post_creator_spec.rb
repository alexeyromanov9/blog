require 'rails_helper'

describe PostCreator do
  let(:user) { User.create(login: 'login') }

  let(:valid_params) { { title: 'title', body: 'body', login: user.login, ip: '194.158.213.76' } }
  let(:params_without_title) { { body: 'body', login: user.login, ip: '194.158.213.76' } }
  let(:params_without_body) { { title: 'title', login: user.login, ip: '194.158.213.76' } }
  let(:params_without_login) { { title: 'title', body: 'body', ip: '194.158.213.76' } }
  let(:params_without_ip) { { title: 'title', body: 'body', login: user.login } }

  let(:changed_count_by) { 1 }
  let(:not_changed_count_by) { 0 }

  describe '#call' do
    context 'with valid_params' do
      subject { described_class.new(valid_params).call }
      it { expect{ subject }.to change(Post, :count).by(changed_count_by) }
    end

    context 'with params_without_title' do
      subject { described_class.new(params_without_title).call }
      it { expect{ subject }.to change(Post, :count).by(not_changed_count_by) }
    end

    context 'with params_without_body' do
      subject { described_class.new(params_without_body).call }
      it { expect{ subject }.to change(Post, :count).by(not_changed_count_by) }
    end

    context 'with params_without_login' do
      subject { described_class.new(params_without_login).call }
      it { expect{ subject }.to change(Post, :count).by(not_changed_count_by) }
    end

    context 'with params_without_ip' do
      subject { described_class.new(params_without_ip).call }
      it { expect{ subject }.to change(Post, :count).by(not_changed_count_by) }
    end
  end
end
