require 'rails_helper'

describe RatingsController, type: :controller do
  let(:created_user) { User.create(login: 'user_login') }
  let(:post_params) { { title: 'title', body: 'body', user: created_user, ip: '93.84.12.75', average_rating: 4 } }
  let(:created_post) { Post.create(post_params) }

  let(:rating) { Rating.create(number: 4, post_id: created_post.id) }
  let(:ratings_params) { { post_id: created_post.id, number: 3 } }

  let(:expected_average_rating) { 3.5 }

  describe 'routes' do
    it { should route(:post, '/rating').to(action: :create) }
  end

  describe 'params' do
    it 'checks permitted_params' do
      should permit(:post_id, :number).for(:create, params: ratings_params)
    end
  end

  describe '#create' do
    before { rating }

    it 'returns average_rating' do
      post :create, params: ratings_params
      expect(response.body.to_f).to eq(expected_average_rating)
    end
  end
end
