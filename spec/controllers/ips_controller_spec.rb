require 'rails_helper'

describe IpsController, type: :controller do
  let!(:first_user) { User.create(login: 'login1') }
  let!(:second_user) { User.create(login: 'login2') }
  let!(:ip_address) { IpAddress.create(ip: '194.158.213.76', user_id: first_user.id) }
  let!(:ip_address2) { IpAddress.create(ip: '194.158.213.76', user_id: second_user.id) }

  let(:expected_result) { [["194.158.213.76", ["login1", "login2"]]] }

  describe 'routes' do
    it { should route(:get, '/ips/multiple_users').to(action: :multiple_users) }
  end

  describe '#multiple_users' do
    it 'returns ip with array of users' do
      get :multiple_users
      expect(JSON.parse(response.body)).to eq(expected_result)
    end
  end
end
