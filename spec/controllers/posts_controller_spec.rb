require 'rails_helper'

describe PostsController, type: :controller do
  let(:expected_post_count) { 1 }
  let(:records_count) { 10 }

  let(:created_user) { User.create(login: 'user_login') }

  let(:post_params) { { title: 'title', body: 'body', login: 'login', ip: '93.84.12.75' } }
  let(:first_post_params) { { title: 'title', body: 'body', user: created_user, ip: '93.84.12.75', average_rating: 4 } }
  let(:second_post_params) { { title: 'title', body: 'body', user: created_user, ip: '93.84.12.75', average_rating: 3 } }

  let(:created_first_post) { Post.create(first_post_params) }
  let(:created_second_post) { Post.create(second_post_params) }

  let(:expected_top_method_result) do
    [{"id"=>created_first_post.id, "title"=>"title", "body"=>"body", "average_rating"=>4.0}, {"id"=>created_second_post.id, "title"=>"title", "body"=>"body", "average_rating"=>3.0}]
  end


  describe 'routes' do
    it { should route(:post, '/posts').to(action: :create) }
    it { should route(:get, '/posts/top').to(action: :top) }
  end

  describe 'params' do
    it 'checks permitted_params' do
      should permit(:title, :body, :login, :ip).for(:create, params: post_params)
    end

    it 'checks post_query_params' do
      params = { records_count: records_count }
      should permit(:records_count).for(:top, verb: :get, params: params)
    end
  end

  describe '#create' do
    it 'creates new post' do
      post :create, params: post_params
      expect(Post.count).to eq(expected_post_count)
    end
  end

  describe '#top' do
    before do
      created_user
      created_first_post
      created_second_post
    end

    it 'returns top record by average_rating' do
      get :top, params: { records_count: records_count }
      expect(JSON.parse(response.body)).to eq(expected_top_method_result)
    end
  end
end
