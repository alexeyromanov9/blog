require 'rails_helper'

describe PostForm, type: :model do
  let(:valid_ip_address) { '93.84.12.75' }
  let(:invalid_ip_address) { '93.84.12' }
  let(:created_user) { User.create(login: 'login') }
  let(:post_form_uniq_login) { PostForm.new(login: 'uniq_login', title: 'title', body: 'body', ip: valid_ip_address) }
  let(:post_form) { PostForm.new(login: created_user.login, title: 'title', body: 'body', ip: valid_ip_address) }

  describe 'attributes' do
    it 'has a title with a type check' do
      expect(described_class).to have_attribute(:title).of_type(String)
    end

    it 'has a body with a type check' do
      expect(described_class).to have_attribute(:body).of_type(String)
    end

    it 'has a login with a type check' do
      expect(described_class).to have_attribute(:login).of_type(String)
    end

    it 'has an ip with a type check' do
      expect(described_class).to have_attribute(:ip).of_type(String)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:body) }
    it { should validate_presence_of(:login) }
    it { should validate_presence_of(:ip) }

    it { should allow_value(valid_ip_address).for(:ip) }
    it { should_not allow_value(invalid_ip_address).for(:ip) }
  end

  describe '#login_uniqueness' do
    context 'when login is not uniq' do
      it 'returns login_uniqueness error' do
        created_user
        post_form.valid?
        expect(post_form.errors.full_messages).to eq(['Login is taken already!'])
      end
    end

    context 'when login is uniq' do
      it 'returns an empty array' do
        created_user
        post_form_uniq_login.valid?
        expect(post_form.errors.full_messages).to eq([])
      end
    end
  end
end
