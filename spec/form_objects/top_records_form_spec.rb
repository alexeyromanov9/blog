require 'rails_helper'

describe TopRecordsForm do
  describe 'attributes' do
    it 'has a records_count with a type check' do
      expect(described_class).to have_attribute(:records_count).of_type(Integer)
    end
  end
end
