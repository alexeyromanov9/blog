require 'rails_helper'

describe RatingForm, type: :model do
  let(:user) { User.create(login: 'my_login') }
  let(:post) { Post.create(user_id: user.id, title: 'title', body: 'body', ip: '93.84.12.75') }
  let(:invalid_rating_form) { RatingForm.new(number: 3, post_id: 999) }
  let(:valid_rating_form) { RatingForm.new(number: 3, post_id: post.id) }

  describe 'attributes' do
    it 'has a number with a type check' do
      expect(described_class).to have_attribute(:number).of_type(Integer)
    end

    it 'has a post_id with a type check' do
      expect(described_class).to have_attribute(:post_id).of_type(Integer)
    end
  end

  describe 'validations' do
    it { should validate_inclusion_of(:number).in_range(1..5) }
    it { should validate_presence_of(:post_id) }
  end

  describe 'post_present' do
    context 'with non-existent post_id' do
      it 'returns post_present error' do
        invalid_rating_form.valid?
        expect(invalid_rating_form.errors.full_messages).to eq(["Post is not present!"])
      end
    end

    context 'with existent post_id' do
      it 'return an empty array' do
        valid_rating_form.valid?
        expect(valid_rating_form.errors.full_messages).to eq([])
      end
    end
  end
end
