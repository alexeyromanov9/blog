require 'rails_helper'

describe User, type: :model do
  it { should have_many(:posts).dependent(:destroy) }
  it { should have_many(:ip_addresses).dependent(:destroy) }
end
