require 'rails_helper'

describe Post, type: :model do
  it { should belong_to(:user) }
  it { should have_one(:rating).dependent(:destroy) }
end
